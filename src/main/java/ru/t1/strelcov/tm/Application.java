package ru.t1.strelcov.tm;

import ru.t1.strelcov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        if (parseArgs(args))
            exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            parseArg(scanner.nextLine());
            System.out.println();
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
        System.out.println(TerminalConst.CMD_EXIT + " - Exit program.");
    }

    private static void displayVersion() {
        System.out.println("1.1.0");
    }

    private static void displayAbout() {
        System.out.println("Sla La");
        System.out.println("slala@slala.ru");
    }
}
